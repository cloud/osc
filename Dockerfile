#ARG BASE_IMAGE=registry.centos.org/centos/centos:7
ARG BASE_IMAGE=gitlab-registry.cern.ch/linuxsupport/c8-base
ARG OPENSTACK_RELEASE=ussuri
ARG PKGS="python3-amqp python3-appdirs python3-cachetools python3-dogpile-cache python3-fasteners python3-fixtures python3-kombu python3-munch python3-paste-deploy python3-pbr python3-pyngus python3-qpid-proton python3-requests-kerberos python3-routes python3-tempita python3-vine python3-webob python3-wrapt python-openstackclient-lang python-oslo-concurrency-lang python-oslo-i18n-lang python-oslo-log-lang python-oslo-middleware-lang python-oslo-utils-lang python3-asn1crypto python3-babel python3-barbicanclient python3-cffi python3-chardet python3-cinderclient python3-cliff python3-cliff python3-cmd2 python3-cmd2 python3-contextlib2 python3-cryptography python3-debtcollector python3-debtcollector python3-decorator python3-deprecation python3-eventlet python3-funcsigs python3-futurist python3-glanceclient python3-greenlet python3-heatclient python3-idna python3-ironic-inspector-client python3-ironicclient python3-jeepney python3-jinja2 python3-keyring python3-keystoneauth1 python3-keystoneclient python3-magnumclient python3-manilaclient python3-markupsafe python3-mistralclient python3-monotonic python3-netaddr python3-netifaces python3-neutronclient python3-novaclient python3-octaviaclient python3-openstackclient python3-openstacksdk python3-os-client-config python3-os-service-types python3-osc-lib python3-oslo-concurrency python3-oslo-config python3-oslo-context python3-oslo-i18n python3-oslo-log python3-oslo-messaging python3-oslo-middleware python3-oslo-serialization python3-oslo-service python3-oslo-utils python3-osprofiler python3-pyOpenSSL python3-pyasn1 python3-pyparsing python3-pyperclip python3-requests python3-requestsexceptions python3-rfc3986 python3-secretstorage python3-six python3-six python3-statsd python3-stevedore python3-swiftclient python3-tenacity python3-urllib3 python3-warlock python3-yappi qpid-proton-c krb5-workstation bash-completion jq"

FROM $BASE_IMAGE
LABEL maintainer="cloud-infrastructure-3rd-level@cern.ch"


ARG OPENSTACK_RELEASE
ARG PKGS

RUN _openstack_release="$OPENSTACK_RELEASE" \
    && export pkgs_spaces="$PKGS" \
    && export pkgs_commas=$(echo "$pkgs_spaces" | sed 's/\ /,/g') \
    # OpenStack Repository for selected Release
    && echo "[centos-cloud-openstack-${OPENSTACK_RELEASE}]"                                              >> /etc/yum.repos.d/centos-cloud-openstack.repo \
    && echo "name=Openstack RDO"                                                                         >> /etc/yum.repos.d/centos-cloud-openstack.repo \
    && echo "baseurl=http://linuxsoft.cern.ch/cern/centos/8/cloud/x86_64/openstack-${OPENSTACK_RELEASE}" >> /etc/yum.repos.d/centos-cloud-openstack.repo \
    && echo "enabled=1"                                                                                  >> /etc/yum.repos.d/centos-cloud-openstack.repo \
    && echo "gpgcheck=0"                                                                                 >> /etc/yum.repos.d/centos-cloud-openstack.repo \
    && echo "priority=5"                                                                                 >> /etc/yum.repos.d/centos-cloud-openstack.repo \
    # CERN OpenStack clients rebuilds
    && echo "[cci-openstack-clients]"                                                                                  >> /etc/yum.repos.d/cci-openstack-clients.repo \
    && echo "name=CERN rebuilds for OpenStack clients"                                                                 >> /etc/yum.repos.d/cci-openstack-clients.repo \
    && echo "baseurl=http://linuxsoft.cern.ch/internal/repos/openstackclients-${OPENSTACK_RELEASE}8-stable/x86_64/os/" >> /etc/yum.repos.d/cci-openstack-clients.repo \
    && echo "enabled=1"                                                                                                >> /etc/yum.repos.d/cci-openstack-clients.repo \
    && echo "gpgcheck=0"                                                                                               >> /etc/yum.repos.d/cci-openstack-clients.repo \
    && echo "priority=5"                                                                                               >> /etc/yum.repos.d/cci-openstack-clients.repo \
    && yum install -y --setopt=tsflags=nodocs --disableplugin=protectbase --disablerepo=epel $pkgs_spaces \
    && openstack complete > /etc/profile.d/osc.sh \
    && yum clean all

WORKDIR /tmp

COPY krb5.conf /etc/krb5.conf
copy cloud.sh /etc/profile.d/cloud.sh

CMD [ "/bin/bash", "--rcfile", "/etc/bashrc" ]
